// Back Buttons
const backButtons = Array.from(document.getElementsByClassName('back')); // document.querySelectorAll('button.back')
const back_click = e => history.back();
const append_back_click = item => item.addEventListener('click', back_click, false);

backButtons.forEach(append_back_click);