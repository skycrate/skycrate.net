#!/usr/bin/env node

// Import functionality from Surf
import Surf from "./lib/surf/index.js";
import path from "path";

// Set the current working directory
const DIR = path.resolve(process.cwd(), '.');
// Initiate Surf as a new object
Surf.init({
	// Add static files with mixin and set the direcotories
	...new Surf.mixins.ServeDirectory(DIR + '/static', "/css", "/img", "/vid", "/js"),
	// Render the index page
	"/": res => res.render("index"),
	// Render the portfolio page
	"/home": res => res.render("/"),
	// Render the portfolio page
	"/portfolio": res => res.render("portfolio"),
	// Render the about page
	"/about": res => res.render("about"),
	// Render Graham's profile page
	"/graham": res => res.render("graham"),
	// Render Chris' Profile page
	"/chris": res => res.render("chris"),
	// Render the blog page
	"/blog": res => res.render("blog"),
	// Render the clients page
	"/clients": res => res.render("clients"),
	// Render the contact page
	"/contact": res => res.render("contact"),
	// ICONS (serves all images/icons in the specified directory on the root (such as /favicon.ico):
	...new Surf.mixins.ServeFiles(DIR + "/static/img/icons/alpha-black"),
	//Set the mixin for displaying errors
	...new Surf.mixins.FailSafe((res, req) => res.render("not-found", {
		uri: req.query.uri
	}, '404'))
})
.middleware(
	// Add request logger middleware
	Surf.middleware.request_logger,
	// Add templates middleware and set the path for the template
	Surf.middleware.templates(DIR + "/views", "layout", {
		// Add the page title
		title: "SKYCRATE",
		// Add the page author
		author : "Chris Hogg | Skycrate",
		// Add the page keywords
		keywords : "Skycrate, Ghost, Boo, Atlaas, Technology",
		// Add the page description
		description: "Skycrate provides modern tech solutions with an emphasis on privacy and security",
		// Add the copyright details of the site
		copyright : `&copy; ${new Date().getFullYear()} SKYCRATE LTD`,
		// Add the behaviours file for the site
		behaviours : "/js/behaviours.js"
	})
// Listen for the current port
).listen();
